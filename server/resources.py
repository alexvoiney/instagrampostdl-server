from flask import request
from flask_restful import Resource

from server import q
from server.tasks import get_post_from_shortcode


class DownloadJobList(Resource):
    def post(self):
        data = request.json["data"]
        if data["type"] == "shortcode":
            q.enqueue(get_post_from_shortcode, data["shortcode"])
        return {"status": "accepted"}
