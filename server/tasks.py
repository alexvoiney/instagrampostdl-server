import os

from instaloader import Instaloader, Post
from instaloader.exceptions import BadResponseException

from server import socketio


SOCKET_IO_DOWNLOAD_EVENT = os.getenv("SOCKET_IO_DOWNLOAD_EVENT")


def retrieve_post_from_shortcode(idwl, shortcode):
    return Post.from_shortcode(idwl.context, shortcode)


def get_post_from_shortcode(shortcode):
    idwl = Instaloader()
    idwl.load_session_from_file(os.getenv("INSTA_USER"), os.getenv("INSTA_SESSION"))
    # try to get post
    try:
        post = retrieve_post_from_shortcode(idwl, shortcode)
    except BadResponseException as exc:
        socketio.emit(
            SOCKET_IO_DOWNLOAD_EVENT,
            {"status": "error", "message": f"{exc}", "shortcode": shortcode},
            json=True,
        )
        return False
    socketio.emit(
        SOCKET_IO_DOWNLOAD_EVENT,
        {
            "status": "done",
            "results": [
                {"shortcode": shortcode, "pcaption": post.pcaption, "pic": post.url, "video_url": post.video_url}
            ],
        },
        json=True,
    )
    return True
