import os


CSRF_ENABLED = True
SESSION_COOKIE_SECURE = True
SESSION_COOKIE_HTTPONLY = True
SESSION_COOKIE_SAMESITE = "None"
DEBUG = os.environ.get("FLASK_DEBUG", False)
TESTING = os.environ.get("FLASK_TESTING", False)
CORS_HEADERS = os.environ.get("CORS_HEADERS", "Content-Type")
ALLOWED_ORIGINS = os.environ.get("ALLOWED_ORIGINS", [])
REDIS_HOST = os.environ.get("REDIS_HOST", None)
REDIS_PORT = os.environ.get("REDIS_PORT", None)
REDIS_QUEUE = os.environ.get("REDIS_QUEUE", "default")
SECRET_KEY = os.environ.get("FLASK_SECRET_KEY", "secret")
