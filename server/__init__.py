from flask import Flask
from flask_cors import CORS
from flask_restful import Api
from flask_socketio import SocketIO
from redis import Redis
from rq import Queue

# configure app
app = Flask(__name__)
app.config.from_pyfile("config.py")
app.config.from_envvar("FLASK_APP_SETTINGS")
cors = CORS(app, resources={r"/*": {"origins": app.config.get("ALLOWED_ORIGINS")}})

# configure RQ
redis_host, redis_port = app.config.get("REDIS_HOST", "localhost"), app.config.get(
    "REDIS_PORT", "6379"
)
redis_conn = Redis(redis_host, int(redis_port))
q = Queue(app.config.get("REDIS_QUEUE", "default"), connection=redis_conn)

# configure socketIO
message_queue_url = "redis://" + (
    f"{redis_host}:{redis_port}" if redis_host and redis_port else ""
)
socketio = SocketIO(
    app,
    cors_allowed_origins=app.config.get("ALLOWED_ORIGINS"),
    message_queue=message_queue_url,
)

# define API
import server.resources  # noqa

api = Api(app)
api.add_resource(server.resources.DownloadJobList, "/job/")


if __name__ == "__main__":
    socketio.run(app, host="127.0.0.1")
