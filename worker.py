#!/usr/bin/env python
import os

from dotenv import load_dotenv
from redis import Redis
from rq import Worker

# load env from .env file
load_dotenv()

# create redis connection
redis = Redis(os.getenv("REDIS_HOST"), os.getenv("REDIS_PORT"))

# Start a worker with a custom queue name
worker = Worker([os.getenv("REDIS_QUEUE", "default")], connection=redis)
worker.work()
